
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
02111-1307  USA

#### code: https://bitbucket.org/piciji/denise/src/master/

# copyright holders

* module "driver" is based of "ruby" used in actual higan, it's rewritten
* "Sid" emulation code, especially SID filter, is taken from VICE http://vice-emu.sourceforge.net
* "VIC-II" cycle emulation code is based on VICE implementation http://vice-emu.sourceforge.net
* Dynamic Rate Control (DRC) formula is copyrighted by RetroArch.
* cosine resampler is copyrighted by RetroArch.
* sinc resampler is copyrighted by RetroArch.
* C64 TrueType v1.2.1/Style font is used for listing content in a gui listview
* freetype is a library for opengl to render text fonts https://www.freetype.org/
* bundled opengl Shader were created by following copyright holders: guest(r) - guest.r@gmail.com
* application logo and icon were created by Retrofan
* french translation was created by Ben
* japanese translation was created by Ulgon
* hungarian translation was created by Ferenc
* Chamberlin Filter is copyrighted by Hoxs64.
* fpaq0 - Stationary order 0 file compressor by Matt Mahoney
* P64 format is copyrighted by BeRo
* taken Floppy Sounds from Trackers-World.NET ([Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/)) samples were cutted