
#pragma once

#include <cstdint>

namespace Fonts {

extern uint8_t c64ProMono[28164];

extern uint8_t c64Pro[32680];

}
