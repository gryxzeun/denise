
struct StateFastLayout : GUIKIT::FramedVerticalLayout {
    struct Top : GUIKIT::HorizontalLayout {
        GUIKIT::Label label;
        GUIKIT::LineEdit edit;
		GUIKIT::Button find;
		GUIKIT::Button hotkeys;
        
        Top();
    } top;
    
    GUIKIT::CheckBox autoSaveIdent;
    GUIKIT::ListView listView;
    
    StateFastLayout();
};

struct StateDirectLayout : GUIKIT::FramedHorizontalLayout {
    GUIKIT::Button load;
    GUIKIT::Button save;
    
    StateDirectLayout();
};

struct SettingsLayout : GUIKIT::FramedVerticalLayout {
    
    struct Control : GUIKIT::HorizontalLayout {    
        GUIKIT::Button load;
        GUIKIT::Button save;
        GUIKIT::LineEdit edit;
        GUIKIT::Button create;
        GUIKIT::Button remove;

        Control();
    } control;
    
    struct Active : GUIKIT::HorizontalLayout {
        GUIKIT::Label activeLabel;
        GUIKIT::Label fileLabel;
        GUIKIT::Button standardButton;
        
        Active();
    } active;

    GUIKIT::CheckBox startWithLastConfigCheckbox;
    GUIKIT::ListView listView;
    
    SettingsLayout();
};

struct ConfigurationsFolderLayout : GUIKIT::HorizontalLayout {
    GUIKIT::Label label;
    GUIKIT::LineEdit pathEdit;
    GUIKIT::Button standard;
    GUIKIT::Button select;
    
    ConfigurationsFolderLayout();
};

struct MemoryPatternLayout : GUIKIT::FramedVerticalLayout {
    
    struct FirstLine : GUIKIT::HorizontalLayout {
        
        GUIKIT::Label valueLabel;
        GUIKIT::StepButton valueStepper;
        GUIKIT::Label invertValueEveryLabel;
        GUIKIT::ComboButton invertValueEveryCombo;        
        
        FirstLine();
    } firstLine;
    
    struct SecondLine : GUIKIT::HorizontalLayout {
        
        GUIKIT::Label lengthRandomLabel;
        GUIKIT::ComboButton lengthRandomCombo;
        GUIKIT::Label repeatRandomEveryLabel;
        GUIKIT::ComboButton repeatRandomEveryCombo;
        
        SecondLine();
    } secondLine;
    
    struct ThirdLine : GUIKIT::HorizontalLayout {
        GUIKIT::Label randomChanceLabel;
        GUIKIT::StepButton randomChanceStepper;
        
        ThirdLine();
    } thirdLine;

    struct FourthLine : GUIKIT::HorizontalLayout {
        GUIKIT::Button preConfigured1;
        GUIKIT::Button preConfigured2;

        FourthLine();
    } fourthLine;
    
    GUIKIT::MultilineEdit preview;
    
    MemoryPatternLayout(TabWindow* tabWindow);
};

struct ConfigurationsLayout : GUIKIT::HorizontalLayout {
    
    TabWindow* tabWindow;
    Emulator::Interface* emulator;    
    
    GUIKIT::FramedVerticalLayout moduleFrame;    
    GUIKIT::ListView moduleList;
        
    GUIKIT::SwitchLayout moduleSwitch;
    
    GUIKIT::VerticalLayout settingsFrame;
    SettingsLayout settings;
    ConfigurationsFolderLayout settingsFolder;
    
    GUIKIT::VerticalLayout statesFrame;
    StateFastLayout stateFast;
    StateDirectLayout stateDirect;
    ConfigurationsFolderLayout stateFolder;
    
    MemoryPatternLayout* memoryPattern = nullptr;

    GUIKIT::Image settingsImage;
    GUIKIT::Image scriptImage;
    GUIKIT::Image memImage;
    
    struct SettingLine {
        std::string fileName;
        std::string date;

        SettingLine(std::string fileName, std::string date)
        : fileName(fileName), date(date) {
        }

        bool operator < (const SettingLine& line) const {
            return fileName < line.fileName;
        }
    };
    
    struct StateLine {
		unsigned pos;
		std::string fileName;
		std::string date;

		StateLine(unsigned pos, std::string fileName, std::string date)
			: pos(pos), fileName(fileName), date(date) {}

		bool operator < (const StateLine& line) const {
			return pos < line.pos;
		}
	}; 
    
    auto translate() -> void;
    auto getSettingsFolder( bool createFolder = false ) -> std::string;
    auto updateSettingsList() -> void;
    auto updateSaveIdent( std::string fileName ) -> void;
    auto splitFile( std::string file, unsigned& pos ) -> std::string;
    auto load( std::string path ) -> bool;
    auto loadSettings() -> void;
    auto updateMemoryPreview() -> void;
    
    ConfigurationsLayout(TabWindow* tabWindow); 
};
